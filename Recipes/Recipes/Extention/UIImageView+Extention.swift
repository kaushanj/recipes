//
//  UIImage+Extention.swift
//  Recipes
//
//  Created by Kaushant on 11/30/18.
//  Copyright © 2018 Riddlefix. All rights reserved.
//

import UIKit
import CommonCrypto


class CustomImageView: UIImageView {
    
    private var imageUrlString: String?
    
    func loadImage(urlString: String, completionHandler: (() -> ())?) -> Void {
        
        self.image = nil
        self.imageUrlString = urlString
        
        ImageCacheService.share.loadImage(key: urlString) { [weak self] (cacheImage) in
            if let cacheImage = cacheImage {
                DispatchQueue.main.async {
                    self?.image = cacheImage
                    return
                }
               
            }
            else {
                NetworkManager.shared.downloadImageWith(urlString: urlString, completionHandler: { (img) in
                    if let cacheImage = img {
                        
                        DispatchQueue.main.async {
                            if self?.imageUrlString == urlString {
                                self?.image = cacheImage
                                ImageCacheService.share.saveImage(key: urlString, image: cacheImage)
                            }
                        }
                        
                    }
                }, errorHandler: { (errorString) in
                    print(errorString)
                })

            }
        }
        
        
    }
}

class ImageCacheService: NSObject {
    
    private static var main: ImageCacheService?
    private let imageCacheTime = 1800
    
    
    static var share: ImageCacheService {
        guard let privateShare = main else {
            main = ImageCacheService()
            return main!
        }
        return privateShare
    }
    
    private var fileManager: FileManager!
    private var imageLocalPath: URL!
    private let imageCache = NSCache<NSString, UIImage>()
    private let imageLoadQue = DispatchQueue(label: "loadimage")
    
    private override init() {
        super.init()
        self.fileManager = FileManager.default
        
        do {
            let documentDirectory = try fileManager.url( for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true )
            self.imageLocalPath = documentDirectory.appendingPathComponent("RecipesCache")
            
            try createDirectory()
            let deadlineTime = DispatchTime.now() + .seconds(imageCacheTime)
            DispatchQueue.global().asyncAfter(deadline: deadlineTime) {
                self.clearCache()
            }
        } catch {
            fatalError()
        }
        
    }
    
    func loadImage(key: String, imageCompletion: @escaping (UIImage?) -> Void ) -> Void {
        let key = MD5(string: key)
        
        self.imageLoadQue.async {
            if let image = self.imageCache.object(forKey: NSString(string: key)) {
                imageCompletion(image)
                return
            }
            
            do {
                let imageData = try Data(contentsOf: self.imageLocalPath.appendingPathComponent(key))
                let image = UIImage(data: imageData)
                imageCompletion(image)
                
            }
            catch {
                imageCompletion(nil)
            }
        }
    }
    
    func saveImage(key: String, image: UIImage) -> Void {
        let key = MD5(string: key)
        self.imageLoadQue.async {
            self.imageCache.setObject(image, forKey: NSString(string: key))
            if let imageData = image.pngData() {
                try? imageData.write(to: self.imageLocalPath.appendingPathComponent(key))
            }
            
        }
        
    }
    
    
    private func MD5(string: String) -> String {
        let messageData = string.data(using:.utf8)!
        var digestData = Data(count: Int(CC_MD5_DIGEST_LENGTH))

        _ = digestData.withUnsafeMutableBytes {digestBytes in
            messageData.withUnsafeBytes {messageBytes in
                CC_MD5(messageBytes, CC_LONG(messageData.count), digestBytes)
            }
        }
        
        return digestData.map { String(format: "%02hhx", $0) }.joined()
    }
    
    private func createDirectory() throws {
        
        if !fileManager.fileExists(atPath: imageLocalPath.path) {
            try fileManager.createDirectory(at: imageLocalPath, withIntermediateDirectories: false, attributes: nil)
        }
        
    }
    
    private func clearCache() {
        try? self.fileManager.removeItem(at: imageLocalPath)
        self.imageCache.removeAllObjects()
        ImageCacheService.main  = nil
    }

}

