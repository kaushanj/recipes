//
//  Ingredient.swift
//  Recipes
//
//  Created by Kaushant on 11/29/18.
//  Copyright © 2018 Riddlefix. All rights reserved.
//

import Foundation

struct Ingredient: Codable {
    var name: String
    var quantity: String
    var type: String
    
    enum Ingredient: CodingKey {
        case name, quantity, type
    }
}
