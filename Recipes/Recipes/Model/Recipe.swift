//
//  Recipe.swift
//  Recipes
//
//  Created by Kaushant on 11/29/18.
//  Copyright © 2018 Riddlefix. All rights reserved.
//

import Foundation

struct Recipe: Codable {
    var name: String
    var originalURL: String?
    var steps: [String]
    var timers: [Int]
    var imageURL: String
    var ingredients: [Ingredient]
    
    enum Recipe: CodingKey {
        case name, steps, imageURL, ingredients, originalURL, timers
    }
}


