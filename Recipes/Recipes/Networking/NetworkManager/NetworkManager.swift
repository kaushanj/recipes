//
//  NetworkManager.swift
//  Recipes
//
//  Created by Kaushant on 11/29/18.
//  Copyright © 2018 Riddlefix. All rights reserved.
//

import UIKit

public typealias ErrorHandler = (String) -> Void

protocol NetworkTask {
    func getRecipes( success: @escaping  ([Recipe])->Void, errorHandler: @escaping  ErrorHandler)
    func downloadImageWith(urlString: String, completionHandler: @escaping ((UIImage?)->Void), errorHandler: @escaping ErrorHandler)
}

class NetworkManager: Router, NetworkTask {
    
    func getRecipes(success: @escaping ([Recipe]) -> Void, errorHandler: @escaping  (String) -> Void) {
        get(urlString: Url.recipesUrl.rawValue, success: success, errorHandler: errorHandler)
    }
    
    func downloadImageWith(urlString: String, completionHandler: @escaping ((UIImage?) -> Void), errorHandler: @escaping ErrorHandler) {
        downloadImage(urlString: urlString, completionHandler: completionHandler, errorHandler: errorHandler)
    }
    
    static let shared:NetworkTask = NetworkManager()
    
    private override init() {
        super.init()
    }
    
    enum Url: String {
        case recipesUrl = "https://private-e27cf-recipes14.apiary-mock.com/recipes"
    }
}
