//
//  Route.swift
//  Recipes
//
//  Created by Kaushant on 11/29/18.
//  Copyright © 2018 Riddlefix. All rights reserved.
//

import UIKit


class Router {
    
    private var task: URLSessionDataTask?
    
    
    func get<T: Decodable>(urlString: String, success: @escaping ([T]) -> Void, errorHandler: @escaping ErrorHandler ){
        
        guard let url = URL(string: urlString) else {
            errorHandler(ErrorTypes.UrlError.rawValue)
            return
        }
        
        request(url: url, method: .get, completionHandler: { (data, error) in
            
            if let error = error {
                errorHandler(error.localizedDescription)
                return
            }
            
            guard let data = data else {
                errorHandler(ErrorTypes.resposeError.rawValue)
                return
            }
            do {
                let decoder = JSONDecoder()
                let responseObject = try decoder.decode([T].self, from: data)
                success(responseObject)
                
            }
            catch {
                errorHandler(error.localizedDescription)
            }
            
        })
    }
    
    func downloadImage(urlString: String, completionHandler: @escaping ((UIImage?)->Void), errorHandler: @escaping ErrorHandler) {
        guard let url = URL(string: urlString) else {
            errorHandler(ErrorTypes.UrlError.rawValue)
            return
        }
        request(url: url) { (data, error) in
            if let error = error {
                errorHandler(error.localizedDescription)
                return
            }
            
            if let imageData = data {
                let image = UIImage(data: imageData)
                completionHandler(image)
            }
        }
    }
    
    private func request(url: URL, method: HTTPMethod = .get, completionHandler: @escaping ((_ data :Data?  , _ error: Error?) -> Void) ) {
        
        
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        
        task = URLSession.shared.dataTask(with: request) { (data, response, err) in
            completionHandler(data, err)
        }
        task?.resume()
        
    }
    
    func cancelTask() -> Void {
        self.task?.cancel()
    }
    
    
    
}

extension Router {
    
    enum ErrorTypes: String {
        case UrlError = "URL Error"
        case resposeError = "SomeThing went worng, please try again later"
    }
    
    enum HTTPMethod: String {
        case get = "GET"
    }
}

