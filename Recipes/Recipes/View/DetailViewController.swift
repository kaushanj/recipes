//
//  DetailViewController.swift
//  Recipes
//
//  Created by Kaushant on 11/30/18.
//  Copyright © 2018 Riddlefix. All rights reserved.
//

import UIKit

class DetailViewController: BaseViewController {
    
    private weak var viewModel: RecipeViewModel!
    
    
    lazy var mainImageview: CustomImageView = {
        let imgv = CustomImageView()
        imgv.contentMode = .scaleAspectFill
        imgv.backgroundColor = .red
        imgv.loadImage(urlString: viewModel.recipe.imageURL, completionHandler: nil)
        imgv.clipsToBounds = true
        return imgv
    }()
    
    lazy var titleLbl: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.adjustsFontSizeToFitWidth = true
        label.textAlignment = .center
        label.sizeToFit()
        label.font = UIFont(name: "Thonburi", size: 32)
        label.text = self.viewModel.recipe.name
        return label
    }()
    
    lazy var titleContainerView: UIView = {
        let view = UIView()
        return view
    }()
    
    lazy var detailTableView: UITableView = {
        let tblView = UITableView()
        tblView.register(IngrediantCell.self, forCellReuseIdentifier: String(describing: IngrediantCell.self))
        tblView.register(InstractionCell.self, forCellReuseIdentifier: String(describing: InstractionCell.self))
        tblView.dataSource = self
        tblView.delegate = self
        return tblView
    }()
    
    convenience init(viewModel: RecipeViewModel){
        self.init()
        self.viewModel = viewModel
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.transitionCoordinator?.animate(alongsideTransition: { [weak self](context) in
            self?.navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
            self?.navigationController?.navigationBar.shadowImage = nil
            }, completion: nil)
    }
    
    override func setupView() {
        super.setupView()
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        
        let backButton = UIBarButtonItem(image: UIImage(named: "back-icon"), style: .plain, target: self, action: #selector(DetailViewController.goBack(_:)))
        backButton.tintColor = .blue
        self.navigationItem.leftBarButtonItem = backButton
        
        
        self.view.addSubview(self.mainImageview)
        self.mainImageview.snp.makeConstraints { (make) in
            make.top.left.right.equalToSuperview()
            make.height.equalToSuperview().multipliedBy(0.4)
        }
        
        self.view.addSubview(titleContainerView)
        self.titleContainerView.snp.makeConstraints { (make) in
            make.top.equalTo(self.mainImageview.snp.bottom)
            make.left.right.equalToSuperview()
            make.height.equalToSuperview().multipliedBy(0.1)
        }
        
        self.titleContainerView.addSubview(self.titleLbl)
        self.titleLbl.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
        }
        
        self.view.addSubview(self.detailTableView)
        self.detailTableView.snp.makeConstraints { (make) in
            make.top.equalTo(self.titleContainerView.snp.bottom)
            make.left.right.equalTo(self.view)
            make.bottom.equalTo(self.view.snp.bottomMargin)
        }
        
    }
    
    @objc func goBack(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }

}


extension DetailViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return self.viewModel.recipe.ingredients.count
        case 1:
            return self.viewModel.recipe.steps.count
        default:
            break
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       
        if let recipe = self.viewModel.recipe {
            
            switch indexPath.section {
            case 0:
                if let ingrediantCell = tableView.dequeueReusableCell(withIdentifier: String(describing: IngrediantCell.self), for: indexPath) as? IngrediantCell {
                    ingrediantCell.ingrediant = recipe.ingredients[indexPath.item]
                    
                    return ingrediantCell
                }
            case 1:
                if let instractionCell = tableView.dequeueReusableCell(withIdentifier: String(describing: InstractionCell.self), for: indexPath) as? InstractionCell {
                    instractionCell.instraction = recipe.steps[indexPath.item]
                    return instractionCell
                }
            default:
                break
            }
            
            
        }
        return UITableViewCell()
        
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        switch section {
        case 0:
            return "\(self.viewModel.recipe.ingredients.count) ingredients"
        case 1:
            return "Instruction"
        default:
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
