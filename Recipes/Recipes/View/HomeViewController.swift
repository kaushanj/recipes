//
//  HomeViewController.swift
//  Recipes
//
//  Created by Kaushant on 11/29/18.
//  Copyright © 2018 Riddlefix. All rights reserved.
//

import UIKit
import SnapKit

class BaseViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
    }
    
    func setupView()  {
        self.view.backgroundColor = .white
    }
}

class HomeViewController: BaseViewController {
    
    private var viewModel: HomeViewMode!
    
    private lazy var recipeCollectionView: UICollectionView = {
        
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 0, right: 10)
        layout.minimumLineSpacing = 5
        let cellWidth = (self.view.frame.size.width - 30) / 2
        layout.itemSize = CGSize(width: cellWidth, height: cellWidth * 4 / 3)
        
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.register(RecipeCell.self, forCellWithReuseIdentifier: String(describing: RecipeCell.self))
        cv.backgroundColor = UIColor.white
        cv.delegate = self
        cv.dataSource = self
        return cv
    }()
    
    private lazy var searchBar: UISearchBar = {
        let searchBar = UISearchBar()
        searchBar.placeholder = "Search"
        searchBar.delegate = self
        return searchBar
        
    }()
    
    convenience init(viewModel: HomeViewMode) {
        self.init()
        self.viewModel = viewModel
        self.loadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    override func setupView() {
        super.setupView()
        
        self.view.addSubview(self.recipeCollectionView)
        self.recipeCollectionView.snp.makeConstraints { (make) in
            make.top.equalTo(self.view.snp.topMargin)
            make.left.right.equalTo(self.view)
            make.bottom.equalTo(self.view.snp.bottomMargin)
        }
        
        self.navigationItem.titleView = self.searchBar
       
    }
    
    private func loadData() {
        
        self.viewModel.loadRecipes { [weak self](done) in
            if done {
                self?.recipeCollectionView.reloadData()
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension HomeViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if self.viewModel.searchActive{
            return self.viewModel.filterRecipe.count
        }
        
        return self.viewModel.recipes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: RecipeCell.self), for: indexPath) as! RecipeCell
        var recipe = self.viewModel.recipes[indexPath.item]
        if self.viewModel.searchActive{
            recipe = self.viewModel.filterRecipe[indexPath.item]
        }
        cell.recipeViewModel = recipe
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var selectedRecipe = self.viewModel.recipes[indexPath.item]
        if self.viewModel.searchActive{
            selectedRecipe = self.viewModel.filterRecipe[indexPath.item]
        }
        
        let detailView = DetailViewController(viewModel: selectedRecipe)
        navigationController?.pushViewController(detailView, animated: true)
    }
    
    
}

extension HomeViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
  
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.resignFirstResponder()
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if let searchText = searchBar.text?.trimmingCharacters(in: .whitespaces) {
            self.viewModel.filterRecipe = self.viewModel.filterRecipesBy(searchKey: searchText)
        }
        
        self.recipeCollectionView.reloadData()
    }
    
}
