//
//  IngrediantCell.swift
//  Recipes
//
//  Created by Kaushant on 12/1/18.
//  Copyright © 2018 Riddlefix. All rights reserved.
//

import UIKit

class BaseTabelCell: UITableViewCell {
    
    private(set) lazy var descriptionLbl: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.adjustsFontSizeToFitWidth = true
        label.sizeToFit()
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.setupView()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView () {
        
        self.addSubview(self.descriptionLbl)
        self.descriptionLbl.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(30)
            make.right.bottom.equalToSuperview().offset(-8)
            make.top.equalToSuperview().offset(8)
            make.centerY.equalToSuperview()
            
        }
    }
}

class IngrediantCell: BaseTabelCell {
    
    
    var ingrediant: Ingredient! {
        didSet{
            self.descriptionLbl.text = "\(self.ingrediant.quantity) \(self.ingrediant.name)"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
