//
//  RecipeCell.swift
//  Recipes
//
//  Created by Kaushant on 11/29/18.
//  Copyright © 2018 Riddlefix. All rights reserved.
//

import UIKit


class BaseCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView () {
        
    }
}
class RecipeCell: BaseCell {
    
    var recipeViewModel : RecipeViewModel! {
        didSet{
            self.discriptionLbl.text = self.recipeViewModel.recipeDetail
            self.imageView.loadImage(urlString: self.recipeViewModel.recipe.imageURL, completionHandler: nil)
        }
    }
    
    private lazy var imageView: CustomImageView = {
        let imageView = CustomImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        
        return imageView
    }()
    
    private(set) lazy var detailContainerView: UIView = {
        let view = UIView()
        return view
    }()
    
    private lazy var discriptionLbl: UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        label.adjustsFontSizeToFitWidth = true
        label.textAlignment = .center
        label.sizeToFit()
        return label
    }()

    override func setupView() {
        
        self.addSubview(self.imageView)
        self.imageView.snp.makeConstraints { (make) in
            make.top.left.right.equalTo(self)
            make.height.equalTo(self).multipliedBy(0.8)
        }
        
        self.addSubview(self.detailContainerView)
        self.detailContainerView.snp.makeConstraints { (make) in
            make.bottom.left.right.equalTo(self)
            make.top.equalTo(self.imageView.snp.bottom)
        }
        
        self.detailContainerView.addSubview(self.discriptionLbl)
        self.discriptionLbl.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.left.equalToSuperview().offset(5)
            make.right.equalToSuperview().offset(-5)
        }
        
    }
}
