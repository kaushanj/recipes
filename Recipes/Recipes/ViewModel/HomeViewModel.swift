//
//  RecipeCellViewModel.swift
//  Recipes
//
//  Created by Kaushant on 11/29/18.
//  Copyright © 2018 Riddlefix. All rights reserved.
//

import Foundation

class HomeViewMode: NSObject {
    
    var recipes: [RecipeViewModel] = []
    var filterRecipe:[RecipeViewModel] = []
    
    var searchActive = false {
        didSet{
            self.filterRecipe = []
        }
    }
    
    override init() {
        super.init()
    }
    
    func loadRecipes(completionHandler: @escaping (Bool) -> Void){
        NetworkManager.shared.getRecipes(success: { [weak self](recipes) in
            self?.recipes = recipes.map({ return RecipeViewModel(recipe: $0)})
            DispatchQueue.main.async {
                completionHandler(true)
            }
            }, errorHandler: { (errprString) in
                print(errprString)
                completionHandler(false)
        })
    }
    
    
    func filterRecipesBy(searchKey: String) -> [RecipeViewModel] {
        let searchList = self.recipes.filter { (recipeModel) -> Bool in
            let isMatchingName = recipeModel.recipe.name.lowercased().contains(searchKey.lowercased())
            let matchingIngredient = recipeModel.recipe.ingredients.filter({ (ingredient) -> Bool in
                let isMatchingIngredientName = ingredient.name.lowercased().contains(searchKey.lowercased())
                let isMatchingIngredienttype = ingredient.type.lowercased().contains(searchKey.lowercased())
                return isMatchingIngredientName || isMatchingIngredienttype
            })
            return isMatchingName || matchingIngredient.count > 0
        }
        
        
        if(searchList.count == 0 && searchKey.isEmpty){
            self.searchActive = false
            return []
        }
        
        self.searchActive = true
        return searchList
    }
    
    
}
