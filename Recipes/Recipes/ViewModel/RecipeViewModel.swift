//
//  RecipeViewModel.swift
//  Recipes
//
//  Created by Kaushant on 11/29/18.
//  Copyright © 2018 Riddlefix. All rights reserved.
//

import UIKit



class RecipeViewModel {
    
    private(set) var recipe: Recipe!
    var recipeDetail: String {
        get{
            return self.setDetail()
        }
    }
    private init() { }
    
    convenience init(recipe: Recipe) {
        self.init()
        self.recipe = recipe
    }
    
    private func setDetail() -> String {
        let title = self.recipe.name
        let intigration = "\(self.recipe.ingredients.count)"
        
        let timeCount: Int = Int(self.recipe.timers.reduce(0, +))
        let minutes = "\(timeCount / 60)"
        
        return "\(title), {\(intigration)} ingredients, {\(minutes)} minutes"
    }
    
}



